# Generated by Django 3.1.2 on 2021-03-29 12:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='inputan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Mata_Kuliah', models.CharField(max_length=50)),
                ('Dosen', models.CharField(max_length=50)),
                ('SKS', models.CharField(max_length=3)),
                ('Semester', models.CharField(max_length=50)),
                ('Kelas', models.CharField(max_length=50)),
                ('Deskripsi', models.CharField(default='-', max_length=100)),
            ],
        ),
    ]
